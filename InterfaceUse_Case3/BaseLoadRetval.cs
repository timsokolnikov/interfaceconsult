﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfaceUse_Case3
{
    abstract class BaseLoadRetval : ILoadRetval
    {
        protected string mContent;
        public string Content
        {
            get
            {
                return mContent;
            }
            set
            {
                mContent = value;
            }
        }
    }
}
