﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfaceUse_Case3
{
    interface ILoader
    {
        ILoadRetval Load(string url);
    }
}
