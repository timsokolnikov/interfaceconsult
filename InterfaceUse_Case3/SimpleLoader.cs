﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfaceUse_Case3
{
    class SimpleLoader:ILoader
    {
        public ILoadRetval Load(string url)
        {
            ILoadRetval retVal = new SimpleLoadRetval();
            (retVal as SimpleLoadRetval).Content = "Loaded webpage content from " + url;
            return retVal;
        }
    }
}
