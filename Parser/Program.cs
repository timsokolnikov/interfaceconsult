﻿using InterfaceUse_Case2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parser
{
    class Program
    {
        static void Main(string[] args)
        {
            ILoader loader = new SimpleLoader();

            Console.WriteLine(loader.Load("C:").Content);
        }
    }
}
