﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfaceUse_Case2
{
    public class SimpleLoader:ILoader
    {
        public LoadRetvalBase Load(string url)
        {
            LoadRetvalBase retVal = new SimpleLoadRetval();
            (retVal as SimpleLoadRetval).Content = "Loaded webpage content from " + url;
            return retVal;
        }
    }
}
