﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArchTricks1.DifferentClassificationFeatures
{
    class BaseDonorOptions
    {
        protected virtual string FilePath { get; set; } // путь к файлу
        protected virtual string Currency { get; set; } // свойство валюта

        public BaseDonorOptions()
        {
            FilePath = "base filepath";
            Currency = "base currency";
        }
    }
}
